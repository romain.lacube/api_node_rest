const mysql = require('mysql');

// var serverIp = '192.168.142.129'; //actia
var serverIp = 'localhost'; //VMWARE
var login = 'devops';
var password = 'devops';
var database = 'devops';
var table = 'user'


var mysqlConnection = mysql.createConnection({
   host: serverIp,
   user: login,
   password: password,
});

mysqlConnection.connect((err) => {
   if (err) throw err;
   else {
      console.log("Connection a MYSQL");
      mysqlConnection.query("CREATE DATABASE IF NOT EXISTS "+ database);
      mysqlConnection.query("USE "+ database + ";");
      mysqlConnection.query("CREATE TABLE IF NOT EXISTS " + table + " (id INT(11) NOT NULL AUTO_INCREMENT, nom VARCHAR(250) NULL," + 
      "prenom VARCHAR(250) NULL, email VARCHAR(250) NULL, jwt VARCHAR(250) NULL, password VARCHAR(250) NULL," +
      "isActivated BOOLEAN DEFAULT false, temporaryPwd INT(11) NULL, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP , CONSTRAINT id_PK PRIMARY KEY (id));");
   }
})

exports.mysqlConnection = mysqlConnection;
exports.ip = serverIp;
