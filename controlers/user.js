var config = require('../model/config')
var bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
var mail = require('./mail');


// mail.sendMail("romain.lacube@gmail.com", "envoi deuxieme de test", mail.inscriptionText, "www.google.fr");

function getRandomInt(max) {
   return Math.floor(Math.random() * Math.floor(max));
}


/**
 * Get all users
 */
exports.getAllUsers = function (req, res) {
   config.mysqlConnection.query('SELECT * FROM user', (err, rows, fields) => {
      if (!err) {
         if (rows.length > 0) {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.statusCode = 200;
            res.send(rows);
         } else {
            res.send(JSON.stringify("Aucun utilisateur"));
         }
      } else {
         console.log(err);
      }
   });
};

/**
 * get one user
 */
exports.getOneUser = function (req, res) {
   config.mysqlConnection.query("SELECT * FROM user WHERE jwt = '" + req.body.jwt + "' LIMIT 1", (err, rows, fields) => {
      if (!err) {
         if (rows.length > 0) {
            res.statusCode = 200;
            res.json({
               status: "ok",
               jwt: rows[0].jwt,
               error: null,
               isActivated: rows[0].isActivated,
               user: rows[0]
            });
         } else {
            res.json({
               status: "ok",
               jwt: rows[0].jwt,
               error: null,
               isActivated: 0,
               user: null
            });
         }
      } else {
         console.log(err);
      }
   });
};

/**
 * delete one user
 */
exports.delOneUser = function (req, res) {
   config.mysqlConnection.query('DELETE FROM user WHERE email = ?', [req.body.email], (err, rows, fields) => {
      if (!err) {
         if (rows.affectedRows > 0) {
            res.statusCode = 200;
            res.json({
               status: "ok",
               jwt: null,
               error: null,
               isActivated: 0
            })
            mail.sendMail(req.body.email, "Confirmation suppression email", mail.confirmationSuppressionCompte);
         } else {
            res.json({
               status: "non ok",
               jwt: null,
               error: null,
               isActivated: 0
            })
         }
      } else {
         console.log(err);
      }
   });
};



/**
 * Add user
 */
exports.addUser = function (req, res) {
   if (req.method === 'POST') {
      res.setHeader('Access-Control-Allow-Origin', '*');
      console.log(req.body)
      // verif si email deja present en BDD
      config.mysqlConnection.query("SELECT password, email FROM user WHERE email = \"" + [req.body.email] + "\" LIMIT 1;", (err, rows, fields) => {
         if (!err) {
            if (rows.length === 0) {
               // génération password Temporarire
               var pwdTempo = getRandomInt(10000) * getRandomInt(10000);

               // cryptage mot de passe avec bcrypt
               bcrypt.genSalt(saltRounds, function (err, salt) {
                  bcrypt.hash([req.body.password].toString(), salt, function (err, hash) {
                     if (!err) {
                        // JWT
                        var email = req.body.email;
                        jwt.sign({
                           email
                        }, "secretKey", (err, token) => {
                           if (!err) {
                              // ajout utilisateur
                              config.mysqlConnection.query("INSERT INTO user (nom, prenom, email,jwt,password, temporaryPwd) VALUES ('" + [req.body.nom] + "','" + [req.body.prenom] + "','" + [req.body.email] + "', '" + token + "', '" + hash + "', '" + pwdTempo + "');",
                                 (err, rows, fields) => {
                                    if (!err) {
                                       res.statusCode = 200;
                                       res.json({
                                          status: "ok",
                                          jwt: token,
                                          error: null,
                                          isActivated: 0
                                       })
                                       mail.sendMail(req.body.email, "Vérification inscription", mail.inscriptionText, "http://localhost:4200/verification/", pwdTempo);
                                       res.end();

                                    } else {
                                       res.send(err);
                                       res.end();
                                    }
                                 });
                           } else {
                              console.log(err);
                              return 0;
                           }
                        });

                     } else console.log(err);
                  });
               });
            } else {
               res.send(JSON.stringify("Email deja present en BDD"));
            }
         }
      });
   } else {
      res.statusCode = 497;
      res.statusMessage = 'Bad Request';
      res.send(req.err);
   }
}

exports.optionsLogin = function (req, res) {
   console.log(req);
   res.Writer.Header().Set("Access-Control-Allow-Origin", "*");
   res.Writer.Header().Set("Access-Control-Allow-Methods", "*")
   res.Writer.Header().Set("Access-Control-Allow-Headers", "*");
   loginUser();
}


/**
 * login user
 */
exports.loginUser = function (req, res) {
   if (req.method === 'POST') {
      console.log(req.body)
      // verification user present en BDD
      config.mysqlConnection.query("SELECT password, email, jwt, isActivated FROM user WHERE email = \"" + [req.body.email] + "\" LIMIT 1;", (err, rows, fields) => {
         if (!err) {
            console.log(rows);
            if (rows.length > 0) {
               bcrypt.compare([req.body.password].toString(), (rows[0].password), function (error, ress) {
                  if (!error) {
                     if (ress === true) {
                        res.statusCode = 200;
                        res.json({
                           status: "ok",
                           jwt: rows[0].jwt,
                           error: null,
                           isActivated: rows[0].isActivated
                        })
                        res.end();
                     } else {
                        res.json({
                           Status: "Bad password"
                        });
                        res.end();
                     }
                  } else {
                     console.log(error);
                  }
               });
            } else {
               res.send(JSON.stringify("Utilisateur non trouve"));
            }
         } else {
            console.log(err);
         }
      });
   } else {
      res.statusCode = 497;
      res.statusMessage = 'Bad Request';
      res.send(req.err);
   }

}

/**
 * Update User
 */
exports.updateUser = function (req, res) {
   if (req.method === 'PUT') {
      // verification user present en BDD
      config.mysqlConnection.query("SELECT password, email FROM user WHERE email = \"" + [req.body.email] + "\" LIMIT 1;", (err, rows, fields) => {
         if (!err) {
            if (rows.length > 0) {
               res.statusCode = 200;
               res.send(JSON.stringify("OK mais Update a implementer"));
            } else {
               res.statusCode = 403;
               res.statusMessage = 'Not found';
               res.send(JSON.stringify("Error"));
            }
         } else {
            res.send(JSON.stringify(err));
         }
      });
   } else {
      res.statusCode = 497;
      res.statusMessage = 'Bad Request';
      res.send(JSON.stringify("Error"));
   }
}

/**
 * Verif utilisateur
 */
exports.postVerifUser = function (req, res) {
   console.log(req.body);
   config.mysqlConnection.query("SELECT * FROM user WHERE temporaryPwd = \"" + [req.body.url] + "\" LIMIT 1;", (err, rows, fields) => {
      if (!err) {
         if (rows.length > 0) {
            config.mysqlConnection.query("UPDATE user SET isActivated = 1 WHERE id = " + rows[0].id + ";", (error, line, fields) => {
               if (!error) {
                  res.json({
                     status: "ok",
                     jwt: rows[0].jwt,
                     error: null,
                     isActivated: rows[0].isActivated
                  });
                  res.end();
               } else {
                  res.json({
                     status: "non ok",
                     jwt: null,
                     error: "Erreur",
                     isActivated: 0
                  });
                  res.end();
               }
            });

         } else {
            res.json({
               status: "non ok",
               jwt: null,
               error: "Personne non trouve",
               isActivated: 0
            });
            res.end();
         }
      } else {
         console.log(err);
      }
   });
}


/**
 * Demande de réinitialisation de mot de passe
 */

exports.postDemandeResetPwd = function (req, res) {
   console.log(req.body);
   config.mysqlConnection.query("SELECT * FROM user WHERE email = \"" + [req.body.email] + "\" LIMIT 1;", (err, rows, fields) => {
      if (!err) {
         if (rows.length > 0) {
            var pwdTempo = getRandomInt(10000) * getRandomInt(10000);
            config.mysqlConnection.query("UPDATE user SET temporaryPwd = '" + pwdTempo + "' WHERE email = '" + [req.body.email] + "';",
               (error, rows, fields) => {
                  if (!error) {
                     res.statusCode = 200;
                     res.json({
                        status: "ok",
                        jwt: null,
                        error: null,
                        isActivated: 0
                     })
                     mail.sendMail(req.body.email, "Demande de réinitialisation de mot de passe", mail.reinitPwdText, "http://localhost:4200/creation-nouveau-mot-de-passe/", pwdTempo);
                     res.end();
                  } else {
                     res.statusCode = 500;
                     console.log(error);
                  }
               });
         } else {
            res.json({
               status: "non ok",
               jwt: null,
               error: "Personne non trouve",
               isActivated: 0
            });
            res.end();
         }
      } else {
         res.statusCode = 500;
      }
   });
}


/**
 * Mise a jour du mot de passe après réinitialisation
 */
exports.ResetPwd = function (req, res)
{
   console.log(req.body);
   config.mysqlConnection.query("SELECT * FROM user WHERE email = \"" + [req.body.email] + "\" AND temporaryPwd = '" + req.body.temporaryPwd + "'  LIMIT 1;", (err, rows, fields) => {
      if (!err)
      {
         if (rows.length > 0)
         {
            bcrypt.genSalt(saltRounds, function (error, salt) {
               if (!error)
               {
                  //PWD 
                  bcrypt.hash([req.body.password].toString(), salt, function (err, hash) {

                     //JWT
                     var email = req.body.email;
                     jwt.sign({
                        email
                     }, "secretKey", (errJwt, token) => {
                        if (!errJwt)
                        {
                           if (!errJwt) {
                              // mise a jour utilisateur
                              config.mysqlConnection.query("UPDATE user SET password = '" + hash + "', jwt = '" + token + "' WHERE  email = \"" + [req.body.email] + "\" AND temporaryPwd = '" + req.body.temporaryPwd + "'",
                                 (err, rows, fields) => {
                                    if (!err) {
                                       res.statusCode = 200;
                                       res.json({
                                          status: "ok",
                                          jwt: token,
                                          error: "Mise a jour de mot de passe réussi !",
                                          isActivated: 0
                                       })
                                       res.end();

                                    } else {
                                       res.send(err);
                                       res.end();
                                    }
                                 });
                           } else {
                              console.log(err);
                              return 0;
                           }
                        }
                     });
                  });
               } else
               {
                  res.statusCode = 500;
                  console.log(error)
               }              
            })
         } else
         {
            res.json({
               status: "non ok",
               jwt: null,
               error: "Etes-vous sur le l'email entré ?",
               isActivated: 0
            });
            res.end();
         }
      } else 
      {
         res.statusCode = 500;
         console.log(err);
      }
   });
}