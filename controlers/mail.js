const nodemailer = require("nodemailer");




exports.sendMail = function (email, motifEnvoiMail, corsText, link='', temporaryPwd='') {

   var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
         user: 'monapplicationperso2019@gmail.com',
         pass: 'MdpSuperSecurePerso2019'
      }
   });
   const mailOptions = {
      from: 'monapplicationperso2019@gmail.com', // sender address
      to: email, // list of receivers
      subject: motifEnvoiMail, // Subject line
      html: corsText + link + temporaryPwd// plain text body
   };

   transporter.sendMail(mailOptions, function (err, info) {
      if (err)
         console.log(err)
      else
         console.log(info);
   });
}


exports.inscriptionText = "<h1> Merci de vous être inscrit sur ma plateforme ! </h1>" +
   "</br> <p>Afin de compléter votre inscription, merci de valider votre inscription en cliquant ici :</p>";
exports.reinitPwdText = "<h1> Voici la procédure à suivre pour réinitialiser le mot de passe : </h1>" +
   "</br> <p>Afin de le réinitialiser, merci de cliquer sur ce lien, vous serez amené sur une page vous permettant de créer un nouveau de mot passe :</p>";
exports.confirmationSuppressionCompte = "<h1> Confirmation de suppression de compte validée </h1>" +
   "</br> <p>L'ensemble des informations de compte compte ont été supprimés.</p>";