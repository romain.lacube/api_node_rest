var user = require('../controlers/user');
var ip = require('../model/config')

module.exports = function (app) {
   app.route('/users')
      .get(user.getAllUsers); // avoir la liste complete de l'ensemble des utilisateurs

   app.route('/user/suppressionProfil')
   .post(user.delOneUser); // suppression utilisateur
   
   app.route('/user/affichageProfil')
   .post(user.getOneUser) // recevoir les informations d'un utilsateur

   app.route('/user')
      .post(user.addUser); // ajout utilisateur

   app.route('/user/login')
      .post(user.loginUser); // login

   app.route('/user/update')
      .put(user.updateUser) // mise a jour profil utilisateur deja present en bdd

   app.route('/user/verification')
      .post(user.postVerifUser); // verification retour utilisateur

   app.route('/user/demandeResetPwd')
      .post(user.postDemandeResetPwd); // demande de réinitialisation de mot de passe

   app.route('/user/ResetPwd')
      .post(user.ResetPwd); // demande de réinitialisation de mot de passe

   // 404
   app.use(function (req, res, next) {
      return res.status(404).send({
         message: 'Route : ' + ip.ip + req.url + ' inexistante.'
      });
   });

   // 500 - serveur error
   app.use(function (err, req, res, next) {
      return res.status(500).send({
         error: err
      });
   });

}