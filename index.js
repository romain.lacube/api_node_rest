
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');


var app = express();
var routes = require('./routes/routes');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());




routes(app);

app.listen(3000, ()=> {
   console.log('Express running');
   
});

